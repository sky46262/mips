#pragma once
#ifndef STATEMENT_H
#include "parser.h"
#include<map>
#include<algorithm>
#include<iostream>
#define uint unsigned int
#define Register int

const int MEMORY_SIZE = 507904;
extern Register R[35];
extern vector<char*> L;
extern char *memory_start, *text_end, *data_end, *heap;
extern bool isEnded;
enum STATEMENT_TYPE{
	CALC, CMP, MOVE, LOAD, STORE, BRANCH, JUMP, NOP, SYS
};
class Statement {
public:
	STATEMENT_TYPE type;
	Statement(STATEMENT_TYPE _type);
	virtual int exec();
	void writeBack(Register *, int);
	virtual ~Statement(){}
};

// Calculation
enum CALC_TYPE{ADD, SUB, MUL, DIV, XOR, NEG, REM};
class Calc : public Statement {
private:
	CALC_TYPE c_type;
	Register *value1, *dest;
	union Value {
		Register *x;
		int y;
		Value():x(NULL){}
	}value2;
	bool isUnsigned, isWithConst;
public:
	Calc(CALC_TYPE, bool, Register*, Register*, Register* = NULL, int = 0);
	~Calc() {}
	int exec();
};

// Comparison
enum CMP_TYPE{CGT, CGE, CLT, CLE, CEQ, CNEQ};
class Cmp :public Statement{
private:
	CMP_TYPE c_type;
	Register *value1, *dest;
	union Value {
		Register *x;
		int y;
		Value() :x(NULL) {}
	}value2;
	bool isWithConst;
public:
	Cmp(CMP_TYPE, Register*, Register*, Register*, int = 0);
	~Cmp() {}
	int exec();
};

// Branch
enum BRANCH_TYPE{B, BEQ, BNE, BGE, BGT, BLE, BLT };
class Branch :public Statement {
private:
	BRANCH_TYPE b_type;
	uint label;
	Register *value1;
	union Value {
		Register *x;
		int y;
		Value() :x(NULL) {}
	}value2;
	bool isWithConst;
public:
	Branch(BRANCH_TYPE, uint, Register* = NULL, Register* = NULL, int = 0);
	~Branch() {}
	int exec();
};
// Jump
//TODO label: char* -> string
// al:$31 ; r: register
enum JUMP_TYPE{J, JR, JAL, JALR};
class Jump :public Statement {
	JUMP_TYPE j_type;
	struct Dest {
		uint label;
		Register *address;
		Dest():address(NULL){}
		~Dest(){}
	}dest;
	int nextPosition;
public:
	Jump(JUMP_TYPE, int, Register*, uint = -1);
	~Jump() {}
	int exec();
};

struct Address {
	int x;
	class toRegis {
	public:
		Register * pos;
		int del;
	}y;
	Address() = default;
	Address(int a, Register* b = NULL, int c = 0) {
		y.pos = b;
		y.del = c;
		x = a;
	}
};
//Load
//I: const
enum LS_TYPE{ADDRESS, BYTE, HALF, WORD, CONST};
class Load :public Statement {
private:
	LS_TYPE l_type;
	Address address;
	Register *dest;
	uint i;
public:
	Load(LS_TYPE, Register*, Address, uint = 0);
	~Load() {}
	int exec();
};

//Load
class Store :public Statement {
private:
	LS_TYPE s_type;
	Address address;
	Register *src;
public:
	Store(LS_TYPE, Register*, Address);
	~Store() {}
	int exec();
};

// Movement
class Move :public Statement {
private:
	Register *src, *dest;
public:
	Move(Register*, Register*);
	~Move() {}
	int exec();
};
#endif // !STATEMENT_H

