#include "statement.h"
Statement::Statement(STATEMENT_TYPE _type) :type(_type) {}

inline void Statement::writeBack(Register* r, int value) {
	//Write Back
	*r = value;
}
Calc::Calc(CALC_TYPE ct, bool isu, Register* de, Register* src1, Register* src2, int x):Statement(CALC), value1(src1), dest(de), c_type(ct), isUnsigned(isu){
	if (ct == NEG) return;
	if (src2 == NULL) value2.y = x, isWithConst = 1;
	else value2.x = src2, isWithConst = 0;
}
Cmp::Cmp(CMP_TYPE ct, Register* de, Register *src1, Register *src2, int x) : Statement(CMP), value1(src1), dest(de), c_type(ct) {
	if(src2 == NULL) value2.y = x, isWithConst = 1;
	else value2.x = src2, isWithConst = 0;
}
Branch::Branch(BRANCH_TYPE ct, uint la, Register *src1, Register *src2, int x) : Statement(BRANCH), value1(src1), label(la), b_type(ct) {
	if (src1 == NULL) return;
	if (src2 == NULL) value2.y = x, isWithConst = 1;
	else value2.x = src2, isWithConst = 0;
}
Jump::Jump(JUMP_TYPE ct, int pos, Register *src, uint la) : Statement(JUMP), j_type(ct), nextPosition(pos){
	if (src == NULL) dest.label = la;
	else dest.address = src;
}

Load::Load(LS_TYPE ct, Register* de, Address add, uint x):Statement(LOAD), l_type(ct), dest(de){
	if (ct == CONST) i = x;
	else address = add;
}
Store::Store(LS_TYPE ct, Register* sr, Address add) : Statement(LOAD), s_type(ct), src(sr), address(add){}

Move::Move(Register* de, Register* sr): Statement(MOVE), dest(de), src(sr){}

int Statement::exec() {
	if (type == SYS){//syscall
		//decode
		//v0 2 a0 4 a1 5
		int v0 = R[2], a0 = R[4], a1 = R[5];
		int _v0 = v0;
		//execution
		string s;
		switch (v0)
		{
		case 1:
			cout << a0;
			break;
		case 4:
			cout << (char*)(a0 + memory_start);
			break;
		case 5:
			cin >> _v0;
			break;
		case 8:
			cin >> s;
			for (int i = 0; i < std::min(a1 - 1, (int)s.length()); i++) {
				*(char*)(a0 + memory_start) = s[i];
				a0++;
			}
			*(char*)(a0 + memory_start) = 0;
			break;
		case 9:
			if ((heap - memory_start) % 4 != 0) {
				heap = memory_start + ((heap - memory_start) / 4 + 1) * 4;
			}
			_v0 = heap - memory_start;
			heap += a0;
			break;
		case 10:
			isEnded = 1;
			return 0;
			break;
		case 17: 
			isEnded = 1;
			return a0;
			break;
		default:
			break;
		}
		writeBack(&R[2], _v0);
	}
	return 0;
}

int Calc::exec() { 
	//Decode
	int x1 = *value1, y1;
	if (c_type != NEG) {
		if (isWithConst) y1 = value2.y;
		else y1 = *(value2.x);
	}
		
	//Execuction
		if (isUnsigned) {
			uint x = x1, y = y1;
			if (dest != NULL) {
				uint z = 0;
				switch (c_type) {
				case ADD:
					z = x + y;
					break;
				case SUB:
					z = x - y;
					break;
				case MUL:
					z = x * y;
					break;
				case DIV:
					z = x / y;
					break;
				case XOR:
					z = x ^ y;
					break;
				case NEG:
					z = ~x;
					break;
				case REM:
					z = x % y;
					break;
				}
				writeBack(dest, z);
			}
			else {
				uint z1, z2;
				if (c_type == MUL) {
					z1 = (size_t)x * y >> 32;
					z2 = (size_t)x * y ^ (z1 << 32ll);
				}
				else {
					z1 = x % y;
					z2 = x / y;
				}
				writeBack(&R[32], z1);
				writeBack(&R[33], z2);
			}
		}
		else {
			int x = x1, y = y1;
			if (dest != NULL) {
				int z = 0;
				switch (c_type) {
				case ADD:
					z = x + y;
					break;
				case SUB:
					z = x - y;
					break;
				case MUL:
					z = x * y;
					break;
				case DIV:
					z = x / y;
					break;
				case XOR:
					z = x ^ y;
					break;
				case NEG:
					if (isUnsigned) z = ~x;
					else z = -x;
					break;
				case REM:
					z = x % y;
					break;
				}

				writeBack(dest, z);
			}
			else {
				int z1, z2;
				if (c_type == MUL) {
					z1 = (long long)x * y >> 32;
					z2 = (long long)x * y ^ (z1 << 32ll);
				}
				else {
					z1 = x % y;
					z2 = x / y;
				}

				writeBack(&R[32], z1);
				writeBack(&R[33], z2);
			}
		}

	return 0; 
}

int Cmp::exec() { 
	//Decode
	int x = *value1, y;
	if (isWithConst) y = value2.y;
	else y = *(value2.x);

	//Execution
	int z;
	switch (c_type)
	{
	case CGT:
		z = (x > y);
		break;
	case CGE:
		z = (x >= y);
		break;
	case CLT:
		z = (x < y);
		break;
	case CLE:
		z = (x <= y);
		break;
	case CEQ:
		z = (x == y);
		break;
	case CNEQ:
		z = (x != y);
		break;
	default:
		break;
	}
	writeBack(dest, z);
	return 0; 
}

int Branch::exec() { 
	bool z = 0;
	if (b_type == B) {
		z = 1;
	}
	else {
		//Decode
		int x = *value1, y;
		if (isWithConst) y = value2.y;
		else y = *(value2.x);
		//Execution
		switch (b_type)
		{
		case BGT:
			z = (x > y);
			break;
		case BGE:
			z = (x >= y);
			break;
		case BLT:
			z = (x < y);
			break;
		case BLE:
			z = (x <= y);
			break;
		case BEQ:
			z = (x == y);
			break;
		case BNE:
			z = (x != y);
			break;
		default:
			break;
		}
	}
	if (z) {
		writeBack(&R[34], L[label] - memory_start);
	}
	return 0; 
}

int Jump::exec() { 
	//decode
	int add;
	if (j_type == J|| j_type == JAL) add = L[dest.label] - memory_start;
	else add = *dest.address;
	//execution
	if (j_type == J || j_type == JR) {
		writeBack(&R[34], add);
	}
	else 
	if (j_type == JAL || j_type == JALR) {
		writeBack(&R[34], add);
		writeBack(&R[31], nextPosition);
	}

	return 0; 
}

int Load::exec() { 
	//decode

	//excution
	int add;
	if (l_type != CONST) {
		if (address.x == -1) add =(*address.y.pos) + address.y.del;
		else add = L[address.x] - memory_start;
	}
	
	//memory
	int z;
	switch (l_type)
	{
	case ADDRESS:
		z = add;
		break;
	case BYTE:
		z = *(char*)(memory_start + add);
		break;
	case HALF:
		z = *(short int*)(memory_start + add);
		break;
	case WORD:
		z = *(int*)(memory_start + add);
		break;
	case CONST:
		z = i;
		break;
	default:
		break;
	}
	writeBack(dest, z);
	return 0;
}

int Store::exec() {
	//decode
	int z = *src;

	//excution
	int add;
	if (address.x == -1) add = (*address.y.pos) + address.y.del;
	else add = L[address.x] - memory_start;

	//memory
	switch (s_type)
	{
	case BYTE:
		*(memory_start + add) = z;
		break;
	case HALF:
		*(short int*)(memory_start + add) = z;
		break;
	case WORD:
		*(int*)(memory_start + add) = z;
		break;
	default:
		break;
	}
	return 0; 
}

int Move::exec() { 
	//decode
	int z = *src;
	//writeback
	writeBack(dest, z);
	return 0; 
}
