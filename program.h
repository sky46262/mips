#pragma once
#ifndef PROGRAM_H

#include<string>
#include"statement.h"
#define uint unsigned int
#define Register int

void initialize();
void Program(string filename);
void Data(string filename);
int exec();

#endif // !PROGRAM_H