#include "program.h"
#include<iostream>

Register R[35];
vector<char*> L;
//sp 29 fp 30 ra 31 hi,lo 32,33 pc 34
extern map<string, int> L_index;
char *memory_start, *text_end, *data_end, *heap;
bool isEnded = 0;
void initialize() {
	for (int i = 0; i < 35; i++) R[i] = 0;
	memory_start = (char *)malloc(MEMORY_SIZE);
	memset(memory_start, 0, sizeof(char) * MEMORY_SIZE);
	R[29] = MEMORY_SIZE; // sp;
}

void Program(string filename) {
	initialize();
	char *now = memory_start;
 	Parser parser(filename);
	string word;
	bool isText = false;
	while (parser.hasMoreTokens()) {
		Token token = parser.getNextToken();
		word = token.toString();
		if (token.getTokenType() == KEYWORD) {
			int idx;
			for (int i = 0; i < KEYWORD_SIZE; i++)
				if (keywords[i] == word) idx = i;
			//Statement
			Statement *st = NULL;
			if (idx < 9) {
				if (word == ".data") {
					isText = false;
					parser.getNextToken();// \n
				}
				if (word == ".text") {
					isText = true;
					parser.getNextToken();// \n
				}
				continue;
			}
			if (idx >= 9 && idx < 24){
				string sub = word.substr(0, 3);
				//dest
				Token token1 = parser.getNextToken();
				parser.getNextToken();
				//src1
				Token token2 = parser.getNextToken();
				//type
				CALC_TYPE ct;
				//isUnsigned
				bool isu;
				if (word.back() == 'u') isu = true;
				else isu = false;
				if (sub == "add" || sub == "sub" || sub == "xor" || sub == "rem") {
					parser.getNextToken();
					Token token3 = parser.getNextToken();
					parser.getNextToken();
					if (sub == "add") ct = ADD;
					if (sub == "sub") ct = SUB;
					if (sub == "xor") ct = XOR;
					if (sub == "rem") ct = REM;
					if (token3.getTokenType() == INT) 
						st = new Calc(ct, isu, &R[token1.toInt()], &R[token2.toInt()], NULL, token3.toInt());
					else st = new Calc(ct, isu, &R[token1.toInt()], &R[token2.toInt()], &R[token3.toInt()]);
				}
				if (sub == "mul" || sub == "div") {
					if (sub == "mul") ct = MUL;
					else ct = DIV;
					if (parser.getNextToken().toString() == ",") {
						Token token3 = parser.getNextToken();
						parser.getNextToken();
						if (token3.getTokenType() == INT)
							st = new Calc(ct, isu, &R[token1.toInt()], &R[token2.toInt()], NULL, token3.toInt());
						else st = new Calc(ct, isu, &R[token1.toInt()], &R[token2.toInt()], &R[token3.toInt()]);
					}
					else {
						if (token2.getTokenType() == INT)
							st = new Calc(ct, isu, NULL, &R[token1.toInt()], NULL, token2.toInt());
						else st = new Calc(ct, isu, NULL, &R[token1.toInt()], &R[token2.toInt()]);
					}
				}
				if (sub == "neg") {
					parser.getNextToken();// \n
					st = new Calc(NEG, isu, &R[token1.toInt()], &R[token2.toInt()]);
				}
			}
			//cmp
			if (idx >= 24 && idx < 30) {
				//dest
				Token token1 = parser.getNextToken();
				parser.getNextToken();// ,
				//src1 src2
				Token token2 = parser.getNextToken();
				parser.getNextToken();
				Token token3 = parser.getNextToken();
				parser.getNextToken();// \n
				//type
				CMP_TYPE ct;
				if (word[1] == 'g') {
					if (word[2] == 't') 
						ct = CGT;
					else ct = CGE;
				}
				if (word[1] == 'l') {
					if (word[2] == 't')
						ct = CLT;
					else ct = CLE;
					
				}
				if (word[1] == 'e') ct = CEQ;
				if (word[1] == 'n') ct = CNEQ;
				if(token3.getTokenType() == REGISTER)
				st = new Cmp(ct, &R[token1.toInt()], &R[token2.toInt()], &R[token3.toInt()]);
				else st = new Cmp(ct, &R[token1.toInt()], &R[token2.toInt()], NULL, token3.toInt());
			}
			//branch
			if (idx >= 30 && idx < 43) {
				if (word[1] == '\0') {
					st = new Branch(B, parser.getNextToken().toInt());
					parser.getNextToken();
				}
				else {
					//type
					BRANCH_TYPE ct;
					if (word[1] == 'g') {
						if (word[2] == 't')
							ct = BGT;
						else ct = BGE;
					}
					if (word[1] == 'l') {
						if (word[2] == 't')
							ct = BLT;
						else ct = BLE;

					}
					if (word[1] == 'e') ct = BEQ;
					if (word[1] == 'n') ct = BNE;
					Token token1 = parser.getNextToken();
					parser.getNextToken();// ,
					//src1
					Token token2 = parser.getNextToken();
					parser.getNextToken();
					if (word.back() == 'z') 
						st = new Branch(ct, token2.toInt(), &R[token1.toInt()], NULL, 0);
					else {
						Token token3 = parser.getNextToken();
						parser.getNextToken();// \n
						if (token2.getTokenType() == INT) st = new Branch(ct, token3.toInt(), &R[token1.toInt()], NULL, token2.toInt());
						else st = new Branch(ct, token3.toInt(), &R[token1.toInt()], &R[token2.toInt()]);
					}
				}
				
			}
			//jump
			if (idx >= 43 && idx < 47) {
				Token token1 = parser.getNextToken();
				parser.getNextToken();
				if (word == "j" ) st = new Jump(J, 0, NULL, token1.toInt());
				if (word == "jr") st = new Jump(JR, 0, &R[token1.toInt()]);
				if (word == "jal") st = new Jump(JAL, now + sizeof(Statement*) - memory_start, NULL, token1.toInt());
				if (word == "jalr") st = new Jump(JALR, now + sizeof(Statement*) - memory_start, &R[token1.toInt()]);
			}
			
			if (idx >= 47 && idx < 55) {
				LS_TYPE ct;
				switch (word[1]) {
				case 'i':
					ct = CONST;
					break;
				case 'a':
					ct = ADDRESS;
					break;
				case 'b':
					ct = BYTE;
					break;
				case 'h':
					ct = HALF;
					break;
				case 'w':
					ct = WORD;
					break;
				}
				Token token1 = parser.getNextToken();
				parser.getNextToken();
				Token token2 = parser.getNextToken();
				if (word[0] == 'l') {
					if (ct == CONST) {
						st = new Load(ct, &R[token1.toInt()], Address(), token2.toInt());
					}
					else {
						if (token2.getTokenType() == LABEL) st = new Load(ct, &R[token1.toInt()], Address(token2.toInt()));
						else {
							if (token2.getTokenType() == BRACKET)
								st = new Load(ct, &R[token1.toInt()], Address(-1, &R[token2.toInt()], 0));
							else {
								Token token3 = parser.getNextToken();
								st = new Load(ct, &R[token1.toInt()], Address(-1, &R[token3.toInt()], token2.toInt()));
							}
						}
					}
				}
				else {
					if (token2.getTokenType() == LABEL)
						st = new Store(ct, &R[token1.toInt()], Address(token2.toInt()));
					else {
						if (token2.getTokenType() == BRACKET)
							st = new Store(ct, &R[token1.toInt()], Address(-1, &R[token2.toInt()], 0));
						else {
							Token token3 = parser.getNextToken();
							st = new Store(ct, &R[token1.toInt()], Address(-1, &R[token3.toInt()], token2.toInt()));
						}
					}
				}
				parser.getNextToken();
			}
			
			if (idx >= 55 && idx < 58) {
				Token token1 = parser.getNextToken();
				parser.getNextToken();
				if (word == "mfhi") st = new Move(&R[token1.toInt()], &R[32]);
				if (word == "mflo") st = new Move(&R[token1.toInt()], &R[33]);
				if (word == "move") {
					st = new Move(&R[token1.toInt()], &R[parser.getNextToken().toInt()]);
					parser.getNextToken();
				}
			}
			if (idx == 58) st = new Statement(NOP), parser.getNextToken();
			if (idx == 59) st = new Statement(SYS), parser.getNextToken();
			//save
			if (idx >= 9) {
 				*(Statement**)now = st;
				now += sizeof(Statement*);
			}
		}
		else 
			if (token.getTokenType() == LABEL && isText) {
				parser.getNextToken();
				if (word.back() == ':') {
					word.pop_back();
					L[token.toInt()] = now;
				}
			}
	}
	text_end = now;
}

void Data(string filename) {
	Parser parser(filename);
	string word;
	char *now = text_end;
	bool isText = false;
	while (parser.hasMoreTokens()) {
		Token token = parser.getNextToken();
		word = token.toString();
		//char* a = (char*)L["_static_111"];
		if (token.getTokenType() == KEYWORD) {
			int idx;
			for (int i = 0; i < KEYWORD_SIZE; i++)
 				if (keywords[i] == word) idx = i;
			if (idx < 9) {
				if (word == ".data") {
					isText = false;
					parser.getNextToken();// \n
				}
				if (word == ".text") {
					isText = true;
					parser.getNextToken();// \n
				}
				if (word[1] == 'a') {
					if (word == ".align") {
						int num = parser.getNextToken().toInt();
						if ((now - text_end) % (1 << num) != 0) {
							int i = (now - text_end) / (1 << num);
							now = text_end + (i + 1) * (1 << num);
						}
					}
					else {// ascii asciiz
						string str = parser.getNextToken().toString();
						for (uint i = 0; i < str.length(); i++, now++)
							*now = str[i];
						if (word.back() == 'z') {
							*now = 0;
							now++;
						}
					}
					parser.getNextToken();// \n
				}
				if (word == ".space") {
					now += parser.getNextToken().toInt();
					parser.getNextToken();// \n
				}
				if (word == ".byte") {
					char num = parser.getNextToken().toInt();
					while (parser.getNextToken().toString() != "\n") {
						*now = num;
						now++;
						num = parser.getNextToken().toInt();
					}
				}
				if (word == ".half") {
					short int *tmp = (short int*)now;
					short int num = parser.getNextToken().toInt();
					while (parser.getNextToken().toString() != "\n") {
						*tmp = num;
						tmp++;
						now += 2;
						num = parser.getNextToken().toInt();
					}
				}
				if (word == ".word") {
					int *tmp = (int*)now;
					int num = parser.getNextToken().toInt();
					while (parser.getNextToken().toString() != "\n") {
						*tmp = num;
						tmp++;
						now += 4;
						num = parser.getNextToken().toInt();
					}
					*tmp = num;
					tmp++;
					now += 4;
				}
			}
		}
		else 
			if (token.getTokenType() == LABEL && !isText) {
				parser.getNextToken();
				if (word.back() == ':') {
					word.pop_back();
					L[token.toInt()] = now;
				}
			}
	}
	data_end = now;
}

int exec() {
	R[34] = L[L_index["main"]] - memory_start;//address of (Statement*)
	heap = data_end;
	//ofstream ofs("test.err");
	while (R[34] + memory_start < text_end) {
		Statement *st = *(Statement**)(R[34] + memory_start);//Instruction Fetch
		R[34] += sizeof(Statement*);
		int v = st->exec();
		if (isEnded) 
			return v;
	}
	return 0;
}
